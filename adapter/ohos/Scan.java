/*
 * Copyright (c) 2021-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


/**
 * bundle compressor class, compress file and directory.
 *
 */
public class Scan {
    private static final String TRUE = "true";
    private static final String FALSE = "false";
    private static final String LINUX_FILE_SEPARATOR = "/";
    private static final String WIN_FILE_SEPARATOR = "\\";
    private static final String WIN ="Win";
    private static final String OS_NAME = "os.name";
    private static final String DUPLICATE_FOLDER_NAME = "duplicate";
    private static final String BACKUPS = "backups";
    private static final String SUFFIX_FOLDER_NAME = "suffix";
    private static final String FILE_SIZE_FOLDER_NAME = "fileSize";
    private static final String EMPTY_STRING = "";
    private static final String MD5 = "MD5";
    private static final String RESULT_MD5 = "md5";
    private static final String RESULT_SIZE = "size";
    private static final String RESULT_COMPRESS = "compress";
    private static final String RESULT_FILES = "files";
    private static final String RESULT_FILE = "file";
    private static final String RESULT_TOTAL_SIZE = "totalSize";
    private static final String DUPLICATE_DESC = "find the duplicated files";
    private static final String DUPLICATE_PARAM = "--stat-duplicate";
    private static final String SUFFIX_DESC = "show files group by file type[.suffix]";
    private static final String SUFFIX_PARAM = "--stat-suffix";
    private static final String SUFFIX_TYPE_UNKOWN = "umkonwnType";
    private static final String FILE_SIZE_DESC = "find files whose size exceed the limit size";
    private static final String FILE_SIZE_PARAM = "--stat-file-size";
    private static final String FILE_SIZE_PARAM_SEPARATOR = " ";
    private static final String FILE_SIZE_PARAM_VALUE = "%s";
    private static final String FILE_SIZE_RESULT_SIZE = "size";
    private static final String FILE_SIZE_RESULT_FILE = "file";
    private static final String HTML_START = "<!DOCTYPE html><html lang=\"en\">";
    private static final String HTML_END = "</html>";
    private static final String TASK_TYPE = "taskType";
    private static final String TASK_DESC = "taskDesc";
    private static final String PARAM = "param";
    private static final String START_TIME = "startTime";
    private static final String STOP_TIME = "stopTime";
    private static final String PATH = "path";
    private static final String PATH_LIST = "pathList";
    private static final String RESULT = "result";
    private static final String STAT_JSON = "stat.json";
    private static final String STAT_HTML = "stat.html";
    private static final String HAP = ".hap";
    private static final String HSP = ".hsp";
    private static final String STAT_CSS = "stat.css";
    private static final String FILE_TYPE_SO = "so";
    private static final String TMP_FOLDER_NAME = "temporary";
    private static final String LIBS_NAME = "libs";
    private static final int BUFFER_SIZE = 10 * 1024;
    private static final int MD5_BUFFER_SIZE = 1024;
    private static final long ZERO = 0;
    private static final int NUM_ZERO = 0;
    private static final long DUPLICATE_TYPE = 1;
    private static final long SUFFIX_TYPE = 3;
    private static final long FILE_SIZE_TYPE = 2;
    private static final long SHOW_SIZE = 10;
    private static final String HTML_HEAD = "<head><meta charset=\"UTF-8\" name=\"stat\"><title>stat</title><link rel=\"stylesheet\" href=\"./stat.css\"></head>";
    private static final String HTML_BUTTON_SHOW = "<button id=\"show_%s\" type=\"button\" onclick=\"show_%s()\" style=\"display: block\">展开更多</button>";
    private static final String HTML_BUTTON_HIDE = "<button id=\"hide_%s\" type=\"button\" onclick=\"hide_%s()\" style=\"display: none\">收起</button>";
    private static final String HTML_FUNCTION = "<script>\n" +
            "    function show(stat){\n" +
            "        var type = document.getElementsByClassName(stat);\n" +
            "        if (stat === 'fileSize') {\n" +
            "            for (i = 0; i < type.length; i++) {\n" +
            "                type[i].style.display = \"table-row\";\n" +
            "            }\n" +
            "        }\n" +
            "        else {\n" +
            "            for (i = 0; i < type.length; i++) {\n" +
            "                type[i].style.display = \"list-item\";\n" +
            "            }\n" +
            "        }\n" +
            "        var show = document.getElementById(\"show_\" + stat)\n" +
            "        show.style.display = 'none';\n" +
            "        var hide = document.getElementById(\"hide_\" + stat)\n" +
            "        hide.style.display = 'block';\n" +
            "    }\n" +
            "    function hide(stat){\n" +
            "        var type = document.getElementsByClassName(stat);\n" +
            "        for (i = 0; i < type.length; i++) {\n" +
            "            type[i].style.display = \"none\";\n" +
            "        }\n" +
            "        var show = document.getElementById(\"show_\" + stat)\n" +
            "        show.style.display = 'block';\n" +
            "        var hide = document.getElementById(\"hide_\" + stat)\n" +
            "        hide.style.display = 'none';\n" +
            "    }\n" +
            "    function show_duplicate(){\n" +
            "        show(\"duplicate\");\n" +
            "    }\n" +
            "    function hide_duplicate(){\n" +
            "        hide(\"duplicate\");\n" +
            "    }\n" +
            "    function show_fileSize(){\n" +
            "        show(\"fileSize\");\n" +
            "    }\n" +
            "    function hide_fileSize(){\n" +
            "        hide(\"fileSize\");\n" +
            "    }\n" +
            "    function show_suffix(){\n" +
            "        show(\"suffix\");\n" +
            "    }\n" +
            "    function hide_suffix(){\n" +
            "        hide(\"suffix\");\n" +
            "    }\n" +
            "</script>";
    private static final String CSS_TEMPLATE = "#box {\n" +
            "    width: 1200px;\n" +
            "    margin: auto;\n" +
            "}\n" +
            ".boxTable {\n" +
            "    border: 1px solid black;\n" +
            "    width: 100%;\n" +
            "    height: 200px;\n" +
            "}\n" +
            ".layout {\n" +
            "    height: 30px;\n" +
            "}\n" +
            ".key {\n" +
            "    border: 1px solid black;\n" +
            "    width: 10%;\n" +
            " }\n" +
            ".value {\n" +
            "    border: 1px solid black;\n" +
            "    width: 90%;\n" +
            "}\n" +
            ".result {\n" +
            "    height: 30px;\n" +
            "}\n" +
            ".duplicateTable {\n" +
            "    border: 1px solid black;\n" +
            "    width: 100%;\n" +
            "    height: 100px;\n" +
            "}\n" +
            ".duplicateLayout {\n" +
            "    height: 30px;\n" +
            "}\n" +
            ".duplicateKey {\n" +
            "    border: 1px solid black;\n" +
            "    width: 60px;\n" +
            "}\n" +
            ".duplicateValue {\n" +
            "    border: 1px solid black;\n" +
            "    width: 840px;\n" +
            "}\n" +
            ".fileSizeTable {\n" +
            "    border: 1px solid black;\n" +
            "    width: 100%;\n" +
            "    height: 100px;\n" +
            "}\n" +
            ".fileSizeLayout {\n" +
            "    height: 30px;\n" +
            "}\n" +
            ".fileSizeKey {\n" +
            "    border: 1px solid black;\n" +
            "    width: 5%;\n" +
            "}\n" +
            ".fileSizeValue {\n" +
            "    border: 1px solid black;\n" +
            "    width: 95%;\n" +
            "}\n" +
            ".suffixTable {\n" +
            "    border: 1px solid black;\n" +
            "    width: 100%;\n" +
            "    height: 100px;\n" +
            "}\n" +
            ".suffixLayout {\n" +
            "    height: 30px;\n" +
            "}\n" +
            ".suffixKey {\n" +
            "    border: 1px solid black;\n" +
            "    width: 60px;\n" +
            "}\n" +
            ".suffixValue {\n" +
            "    border: 1px solid black;\n" +
            "    width: 840px;\n" +
            "}\n" +
            ".duplicate {\n" +
            "    display: none;\n" +
            "}\n" +
            ".suffix {\n" +
            "    display: none;\n" +
            "}\n" +
            ".fileSize {\n" +
            "    display: none;\n" +
            "    height: 30px;\n" +
            "}\n" +
            "button {\n" +
            "}";
    private static final Log LOG = new Log(Scan.class.toString());

    private static class ParamModel {
        private String md5 = EMPTY_STRING;
        private long size = ZERO;

        private List<String> files = new ArrayList<>();

        public String getMd5() {
            return this.md5;
        }
        public void setMd5(String md5) {
            this.md5 = md5;
        }
        public long getSize() {
            return this.size;
        }
        public void setSize(long size) {
            this.size = size;
        }
        public List<String> getFiles() {
            return this.files;
        }
        public void setFiles(List<String> files) {
            this.files = files;
        }
    }

    private static class ParamModelSuffixSon {
        private String file = EMPTY_STRING;
        private long size = ZERO;

        public String getFile() {
            return this.file;
        }
        public void setFile(String file) {
            this.file = file;
        }
        public long getSize() {
            return this.size;
        }
        public void setSize(long size) {
            this.size = size;
        }

    }
    private static class ParamModelSuffixSo extends ParamModelSuffixSon{
        private String compress = FALSE;
        public String getCompress() {
            return this.compress;
        }
        public void setCompress(String compress) {
            this.compress = compress;
        }
    }

    private static class ParamModelSuffix {
        private String suffix = EMPTY_STRING;
        private long totalSize = ZERO;

        private List files = new ArrayList<>();

        public String getSuffix() {
            return this.suffix;
        }
        public void setSuffix(String suffix) {
            this.suffix = suffix;
        }
        public long getTotalSize() {
            return this.totalSize;
        }
        public void setTotalSize(long totalSize) {
            this.totalSize = totalSize;
        }
        public List getFiles() {
            return this.files;
        }
        public void setFiles(List files) {
            this.files = files;
        }
    }

    private static class DuplicateResult {
        private long taskType = DUPLICATE_TYPE;
        private String taskDesc = DUPLICATE_DESC;
        private String param = DUPLICATE_PARAM;
        private String startTime = EMPTY_STRING;
        private String stopTime = EMPTY_STRING;
        private List<ParamModel> result = new ArrayList<>();
        public long getTaskType() {
            return this.taskType;
        }
        public void setTaskType(long taskType) {
            this.taskType = taskType;
        }
        public String getTaskDesc() {
            return this.taskDesc;
        }
        public void setTaskDesc(String taskDesc) {
            this.taskDesc = taskDesc;
        }
        public String getParam() {
            return this.param;
        }
        public void setParam(String param) {
            this.param = param;
        }
        public String getStartTime() {
            return this.startTime;
        }
        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }
        public String getStopTime() {
            return this.stopTime;
        }
        public void setStopTime(String stopTime) {
            this.stopTime = stopTime;
        }
        public List<ParamModel> getResult() {
            return this.result;
        }
        public void setResult(List<ParamModel> result) {
            this.result = result;
        }
    }

    private static class SuffixResult {
        private long taskType = SUFFIX_TYPE;
        private String taskDesc = SUFFIX_DESC;
        private String param = SUFFIX_PARAM;
        private String startTime = EMPTY_STRING;
        private String stopTime = EMPTY_STRING;
        private List<String> pathList = new ArrayList<>();
        private List<ParamModelSuffix> result = new ArrayList<>();
        public long getTaskType() {
            return this.taskType;
        }
        public void setTaskType(long taskType) {
            this.taskType = taskType;
        }
        public String getTaskDesc() {
            return this.taskDesc;
        }
        public void setTaskDesc(String taskDesc) {
            this.taskDesc = taskDesc;
        }
        public String getParam() {
            return this.param;
        }
        public void setParam(String param) {
            this.param = param;
        }
        public String getStartTime() {
            return this.startTime;
        }
        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }
        public String getStopTime() {
            return this.stopTime;
        }
        public void setStopTime(String stopTime) {
            this.stopTime = stopTime;
        }
        public List<ParamModelSuffix> getResult() {
            return this.result;
        }
        public void setResult(List<ParamModelSuffix> result) {
            this.result = result;
        }
        public List<String> getPathList() {
            return this.pathList;
        }
        public void setPathList(List<String> pathList) {
            this.pathList = pathList;
        }
    }

    private static class ParamModelFileSize {
        private String file = EMPTY_STRING;
        private long size = ZERO;
        public long getSize() {
            return this.size;
        }
        public void setSize(long size) {
            this.size = size;
        }
        public String getFile() {
            return file;
        }
        public void setFile(String file) {
            this.file = file;
        }
    }

    private static class FileSizeResult {
        private long taskType = FILE_SIZE_TYPE;
        private String taskDesc = FILE_SIZE_DESC;
        private String param = FILE_SIZE_PARAM + FILE_SIZE_PARAM_SEPARATOR + FILE_SIZE_PARAM_VALUE;
        private String startTime = EMPTY_STRING;
        private String stopTime = EMPTY_STRING;
        private List<ParamModelFileSize> result = new ArrayList<>();
        public long getTaskType() {
            return this.taskType;
        }
        public void setTaskType(long taskType) {
            this.taskType = taskType;
        }
        public String getTaskDesc() {
            return this.taskDesc;
        }
        public void setTaskDesc(String taskDesc) {
            this.taskDesc = taskDesc;
        }
        public String getParam() {
            return this.param;
        }
        public void setParam(String param) {
            this.param = param;
        }
        public String getStartTime() {
            return this.startTime;
        }
        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }
        public String getStopTime() {
            return this.stopTime;
        }
        public void setStopTime(String stopTime) {
            this.stopTime = stopTime;
        }
        public List<ParamModelFileSize> getResult() {
            return this.result;
        }
        public void setResult(List<ParamModelFileSize> result) {
            this.result = result;
        }
    }

    /**
     * start scan.
     * file orders as follows:
     * for hap: 1.config.json 2.lib 3.res 4.assets 5.*.so 6.*.dex 7.*.apk 8.resources.index
     * for app: 1.certificate 2.signature 3.pack.info 4.hap (1 and 2 may not be used)
     *
     * @param utility common data
     * @return scanProcess if scan succeed
     */
    public boolean scanProcess(Utility utility) {
        File destFile = new File(utility.getOutPath());

        // if out file directory not exist, mkdirs.
        File outParentFile = destFile.getParentFile();
        if ((outParentFile != null) && (!outParentFile.exists())) {
            if (!outParentFile.mkdirs()) {
                LOG.error("Scan::scanProcess create out file parent directory failed.");
                return false;
            }
        }
        boolean scanResult = true;
        try {
            scanExcute(utility);
        } catch (FileNotFoundException exception) {
            scanResult = false;
            LOG.error("Scan::scanProcess file not found exception" + exception.getMessage());
        } catch (BundleException | IOException exception) {
            scanResult = false;
            LOG.error("Scan::scanProcess Bundle exception.");
        }
        // if scan failed, delete out file.
        if (!scanResult) {
            LOG.error("Scan::scanProcess compress failed.");
            if (!destFile.delete()) {
                LOG.error("Scan::scanProcess delete dest file failed.");
            }
        }
        return scanResult;
    }

    private void scanExcute(Utility utility) throws BundleException, IOException {
        List<String> jsonList = new ArrayList<>();
        String htmlStr = HTML_START + HTML_HEAD + "<div id=\"box\">" + "<body>" + HTML_FUNCTION;
        if (TRUE.equals(utility.getStatDuplicate())) {
            String duplicateHtml = statDuplicate(utility, jsonList);
            htmlStr = htmlStr + duplicateHtml;
        }
        if (null != utility.getStatFileSize() && !utility.getStatFileSize().isEmpty()) {
            String fileSizeHtml = statFileSize(utility, jsonList);
            htmlStr = htmlStr + fileSizeHtml;
        }
        if (TRUE.equals(utility.getStatSuffix())) {
            String suffixHtml = statSuffix(utility, jsonList);
            htmlStr = htmlStr + suffixHtml;
        }
        if (!((FALSE.equals(utility.getStatDuplicate())) && FALSE.equals(utility.getStatSuffix())
                && EMPTY_STRING.equals(utility.getStatFileSize()))) {
            htmlStr = htmlStr + "</div>" + "</body>" + HTML_END;
            String jsonPath = utility.getOutPath() + LINUX_FILE_SEPARATOR + STAT_JSON;
            String htmlPath = utility.getOutPath() + LINUX_FILE_SEPARATOR + STAT_HTML;
            String cssPath = utility.getOutPath() + LINUX_FILE_SEPARATOR + STAT_CSS;
            writeFile(jsonPath, jsonList.toString());
            writeFile(htmlPath, htmlStr);
            writeFile(cssPath, CSS_TEMPLATE);
        }
    }

    private List<String> getAllInputFileList(Utility utility, String path) throws BundleException, IOException {
        ArrayList<String> fileList = new ArrayList<>();
        unpackHap(utility.getInput(), path);
        File file = new File(path);
        File[] files = file.listFiles();
        if (files == null) {
            LOG.error("getAllInputFileList: no file in this file path.");
            return fileList;
        }
        String copyPath = path + LINUX_FILE_SEPARATOR + BACKUPS;
        for (File f : files) {
            String fName = f.getName();
            if (fName.endsWith(HSP) || fName.endsWith(HAP)) {
                // copy file and delete file
                String absolutePath = f.getAbsolutePath();
                File destDir = new File(copyPath);
                if (!destDir.exists()) {
                    destDir.mkdirs();
                }
                String targetPath = copyPath + LINUX_FILE_SEPARATOR + fName;
                File targetFile = new File(targetPath);
                File sourceFile = new File(absolutePath);
                FileUtils.copyFile(sourceFile, targetFile);
                deleteFile(absolutePath);
                String outPath = path + LINUX_FILE_SEPARATOR + fName;
                File outDir = new File(outPath);
                if (!outDir.exists()) {
                    outDir.mkdirs();
                }
                unpackHap(targetPath, outPath);
            }
        }

        deleteFile(copyPath);
        FileUtils.getFileList(path, fileList);
        return fileList;
    }

    private String statDuplicate(Utility utility, List<String> jsonList)
            throws BundleException, IOException {
        DuplicateResult duplicateResult = new DuplicateResult();
        // set start time
        duplicateResult.setStartTime(getCurrentTime());
        String currentDir = System.getProperty("user.dir");
        String targetPath = currentDir + LINUX_FILE_SEPARATOR + DUPLICATE_FOLDER_NAME;

        // scan all files
        List<String> fileList = getAllInputFileList(utility, targetPath);
        List<ParamModel> resList = new ArrayList<>();
        for (String filePath: fileList) {
            boolean addFlag = true;
            String md5 = md5HashCode(filePath);
            for (ParamModel element: resList) {
                String eleMd5 = element.getMd5();
                if (eleMd5.equals(md5)) {
                    List<String> eleFiles = element.getFiles();
                    eleFiles.add(cutPath(filePath,DUPLICATE_FOLDER_NAME));
                    element.setFiles(eleFiles);
                    addFlag = false;
                }
            }
            if (addFlag) {
                ParamModel model = new ParamModel();
                long size = FileUtils.getFileSize(filePath);
                model.setMd5(md5);
                model.setSize(size);
                List<String> files = model.getFiles();
                files.add(cutPath(filePath,DUPLICATE_FOLDER_NAME));
                resList.add(model);
            }
        }
        File ParentFile = new File(utility.getOutPath());
        if (!ParentFile.exists()) {
            if (!ParentFile.mkdirs()) {
                LOG.error("Compressor::compressHapAddition create target file parent directory failed.");
            }
        }
        duplicateResult.setResult(resList);
        String jsonStr = JSON.toJSONString(duplicateResult, new SerializerFeature[]{
                SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteDateUseDateFormat});
        // set stop time
        duplicateResult.setStopTime(getCurrentTime());
        String taskTypeHtml = getHtmlRow(TASK_TYPE, duplicateResult.getTaskType());
        String taskDescHtml = getHtmlRow(TASK_DESC, duplicateResult.getTaskDesc());
        String paramHtml = getHtmlRow(PARAM, duplicateResult.getParam());
        String startTimeHtml = getHtmlRow(START_TIME, duplicateResult.getStartTime());
        String stopTimeHtml = getHtmlRow(STOP_TIME, duplicateResult.getStopTime());
        String resultValue = getResultHtml(duplicateResult.getResult());
        String resultHtml = getHtmlRowResultClass(RESULT, resultValue);
        String htmlStr = "<table class=\"boxTable\">" + taskTypeHtml + taskDescHtml + paramHtml +
                startTimeHtml + stopTimeHtml + resultHtml + "</table>";
        deleteFile(targetPath);
        jsonList.add(jsonStr);
        return htmlStr;
    }

    private String statSuffix(Utility utility, List<String> jsonList) throws BundleException, IOException {
        SuffixResult suffixResult = new SuffixResult();
        suffixResult.setStartTime(getCurrentTime());
        String currentDir = System.getProperty("user.dir");
        String targetPath = currentDir + LINUX_FILE_SEPARATOR + SUFFIX_FOLDER_NAME;

        // scan all files
        List<String> fileList = getAllInputFileList(utility, targetPath);
        HashMap<String, List<ParamModelSuffixSon>> hashMap = new HashMap<>();
        for (String filePath: fileList) {
            hashMap = accountFileType(hashMap,filePath);
        }
        Iterator<String> iterator = hashMap.keySet().iterator();
        List<ParamModelSuffix> resulList = new ArrayList<>();
        String outPath = currentDir + LINUX_FILE_SEPARATOR + TMP_FOLDER_NAME;
        String packageName = utility.getInput();
        unpackHap(packageName, outPath);
        List<String> pathList = new ArrayList<>();
        ArrayList<String> soList = new ArrayList<>();
        FileUtils.getFileList(outPath, soList);
        String osName = System.getProperty(OS_NAME);
        File pack =  new File(packageName);
        for (String file: soList) {
            if(file.contains(HAP)||file.contains(HSP)){
                file = cutPath(file,TMP_FOLDER_NAME);
                pathList.add(pack.getName()+file);
            }
        }
        suffixResult.setPathList(pathList);

        while (iterator.hasNext()){
            String next = iterator.next();
            ParamModelSuffix paramModelSuffix = new ParamModelSuffix();
            paramModelSuffix.setSuffix(next);
            if(next.equalsIgnoreCase(FILE_TYPE_SO)){
                List<ParamModelSuffixSo> paramModelSuffixSos = new ArrayList<>();
                List<ParamModelSuffixSon> paramModelSuffixSons = hashMap.get(next);
                long sum = ZERO;
                for(ParamModelSuffixSon param:paramModelSuffixSons){
                    ParamModelSuffixSo paramModelSuffixSo = new ParamModelSuffixSo();
                    paramModelSuffixSo.setSize(param.getSize());
                    String soFilePath = param.getFile();
                    soFilePath = cutPath(soFilePath,SUFFIX_FOLDER_NAME);
                    paramModelSuffixSo.setFile(soFilePath);
                    int i = NUM_ZERO;
                    if(osName.startsWith(WIN)) {
                        i = soFilePath.indexOf(WIN_FILE_SEPARATOR + LIBS_NAME);
                    } else {
                        i = soFilePath.indexOf(LINUX_FILE_SEPARATOR + LIBS_NAME);
                    }
                    String hapPath = soFilePath.substring(NUM_ZERO,i);
                    File hapFile = new File(hapPath);
                    long oldSize = hapFile.length();
                    long newSize = ZERO;
                    for (String file: soList) {
                        File tmp = new File(file);
                        if(tmp.getName().equals(hapFile.getName())){
                            oldSize = tmp.length();
                        }
                    }
                    for(ParamModelSuffixSon so:paramModelSuffixSons){
                        if(so.getFile().contains(hapFile.getPath())){
                            File tmp = new File(so.getFile());
                            newSize+=tmp.length();
                        }
                    }
                    paramModelSuffixSo.setCompress(oldSize < newSize?TRUE:FALSE);
                    paramModelSuffixSos.add(paramModelSuffixSo);
                    sum+=param.getSize();
                }
                deleteFile(outPath);
                paramModelSuffix.setTotalSize(sum);
                paramModelSuffixSos.sort(Comparator.comparing(ParamModelSuffixSo::getSize).reversed());
                paramModelSuffix.setFiles(paramModelSuffixSos);
            }else{
                List<ParamModelSuffixSon> paramModelSuffixSons = hashMap.get(next);
                for(ParamModelSuffixSon son:paramModelSuffixSons){
                    String sonFilePath = son.getFile();
                    son.setFile(cutPath(sonFilePath,SUFFIX_FOLDER_NAME));
                }
                paramModelSuffixSons.sort(Comparator.comparing(ParamModelSuffixSon::getSize).reversed());
                paramModelSuffix.setFiles(paramModelSuffixSons);
                long sum = paramModelSuffixSons.stream().mapToLong(ParamModelSuffixSon::getSize).sum();
                paramModelSuffix.setTotalSize(sum);
            }
            resulList.add(paramModelSuffix);
        }
        resulList = resulList.stream().sorted(Comparator.comparing(
                        ParamModelSuffix::getTotalSize,Comparator.reverseOrder()))
                .collect(Collectors.toList());//sorted by totalSize
        suffixResult.setResult(resulList);
        File ParentFile = new File(utility.getOutPath());
        if (!ParentFile.exists()) {
            if (!ParentFile.mkdirs()) {
                LOG.error("Compressor::compressHapAddition create target file parent directory failed.");
            }
        }
        deleteFile(outPath);
        suffixResult.setStopTime(getCurrentTime());
        String jsonStr = JSON.toJSONString(suffixResult, new SerializerFeature[]{
                SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteDateUseDateFormat});
        String taskTypeHtml = getHtmlRow(TASK_TYPE, suffixResult.getTaskType());
        String taskDescHtml = getHtmlRow(TASK_DESC, suffixResult.getTaskDesc());
        String paramHtml = getHtmlRow(PARAM, suffixResult.getParam());
        String startTimeHtml = getHtmlRow(START_TIME, suffixResult.getStartTime());
        String stopTimeHtml = getHtmlRow(STOP_TIME, suffixResult.getStopTime());
        String pathHtml = EMPTY_STRING;
        if(suffixResult.getPathList() != null && !suffixResult.getPathList().isEmpty()){
            pathHtml = getPathListHtml(suffixResult.getPathList());
        }
        String pathListHtml = EMPTY_STRING;
        if(!pathHtml.isBlank()){
            pathListHtml = getHtmlRow(PATH_LIST, pathHtml);
        }
        String resultValue = getResultHtmlOfSuffix(suffixResult.getResult());
        String resultHtml = getHtmlRowResultClass(RESULT, resultValue);
        String htmlStr =  "<table class=\"boxTable\">" + taskTypeHtml + taskDescHtml + paramHtml +
                startTimeHtml + stopTimeHtml + pathListHtml + resultHtml + "</table>";
        deleteFile(targetPath);
        jsonList.add(jsonStr);
        return htmlStr;
    }

    private String statFileSize(Utility utility, List<String> jsonList) throws BundleException, IOException {
        FileSizeResult fileSizeResult = new FileSizeResult();
        // set start time
        fileSizeResult.setStartTime(getCurrentTime());
        String currentDir = System.getProperty("user.dir");
        String targetPath = currentDir + LINUX_FILE_SEPARATOR + FILE_SIZE_FOLDER_NAME;

        // scan all files
        List<String> fileList = getAllInputFileList(utility, targetPath);
        List<ParamModelFileSize> resList = new ArrayList<>();
        for (String filePath : fileList) {
            long statFileSize = Long.parseLong(utility.getStatFileSize());
            long size = FileUtils.getFileSize(filePath);
            if (size > statFileSize) {
                ParamModelFileSize model = new ParamModelFileSize();
                model.setFile(cutPath(filePath,FILE_SIZE_FOLDER_NAME));
                model.setSize(size);
                resList.add(model);
            }
        }
        // order by size from large to small
        resList.sort(Comparator.comparing(ParamModelFileSize::getSize).reversed());
        fileSizeResult.setResult(resList);
        // set param input size
        fileSizeResult.setParam(String.format(fileSizeResult.getParam(), utility.getStatFileSize()));
        // set stop time
        fileSizeResult.setStopTime(getCurrentTime());
        File ParentFile = new File(utility.getOutPath());
        if (!ParentFile.exists()) {
            if (!ParentFile.mkdirs()) {
                LOG.error("Compressor::compressHapAddition create target file parent directory failed.");
            }
        }
        // output json result
        String jsonStr = JSON.toJSONString(fileSizeResult, new SerializerFeature[]{
                SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteDateUseDateFormat});
        // output html result
        String taskTypeHtml = getHtmlRow(TASK_TYPE, fileSizeResult.getTaskType());
        String taskDescHtml = getHtmlRow(TASK_DESC, fileSizeResult.getTaskDesc());
        String paramHtml = getHtmlRow(PARAM, fileSizeResult.getParam());
        String startTimeHtml = getHtmlRow(START_TIME, fileSizeResult.getStartTime());
        String stopTimeHtml = getHtmlRow(STOP_TIME, fileSizeResult.getStopTime());
        String resultValue = getResultHtmlFileSize(fileSizeResult.getResult());
        String resultHtml = getHtmlRowResultClass(RESULT, resultValue);
        String htmlStr = "<table class=\"boxTable\">" + taskTypeHtml + taskDescHtml + paramHtml +
                            startTimeHtml + stopTimeHtml + resultHtml + "</table>";
        deleteFile(targetPath);
        jsonList.add(jsonStr);
        return htmlStr;
    }
    private String cutPath(String path,String packageName){
        String[] split = path.split(packageName);
        return split[1];
    }
    private static HashMap<String, List<ParamModelSuffixSon>> accountFileType(HashMap<String, List<ParamModelSuffixSon>> hashMap,String f) {
        File file = new File(f);
        String[] split = file.getName().split("\\.");
        if (split.length == 2) {
            if (hashMap.containsKey(split[1])) {
                putValueIntoHashmap(hashMap,file,split[1]);
            } else {
                putValueIntoHashmapnew(hashMap,file,split[1]);
            }
        }else if(split.length == 1) {// no suffix
            if (hashMap.containsKey(SUFFIX_TYPE_UNKOWN)) {
                putValueIntoHashmap(hashMap,file,SUFFIX_TYPE_UNKOWN);
            }else {
                putValueIntoHashmapnew(hashMap,file,SUFFIX_TYPE_UNKOWN);
            }
        }
        return hashMap;
    }

    private static void putValueIntoHashmap(HashMap<String, List<ParamModelSuffixSon>> hashMap,File file,String key){
        ParamModelSuffixSon paramModel2 = setModelValue(file);
        List<ParamModelSuffixSon> paramModel2s = hashMap.get(key);
        paramModel2s.add(paramModel2);
        hashMap.put(key, paramModel2s);
    }

    private static void putValueIntoHashmapnew(HashMap<String, List<ParamModelSuffixSon>> hashMap,File file,String key){
        ParamModelSuffixSon paramModel2 = setModelValue(file);
        List<ParamModelSuffixSon> fileNew = new ArrayList<>();
        fileNew.add(paramModel2);
        hashMap.put(key, fileNew);
    }

    private static ParamModelSuffixSon setModelValue(File file){
        ParamModelSuffixSon paramModel2 = new ParamModelSuffixSon();
        paramModel2.setFile(file.getPath());
        long size = FileUtils.getFileSize(file.getPath());
        paramModel2.setSize(size);
        return paramModel2;
    }

    private static String getResultHtml(List<ParamModel> models) {
        StringBuilder resultHtml = new StringBuilder(EMPTY_STRING);
        resultHtml.append("<ul>");
        for (int i = 0; i < models.size(); i++) {
            ParamModel model = models.get(i);
            String md5Html = getHtmlRowResult(RESULT_MD5, model.getMd5(),
                    " class=\"duplicateLayout\"", " class=\"duplicateKey\"", " class=\"duplicateValue\"");
            String sizeHtml = getHtmlRowResult(RESULT_SIZE, model.getSize(),
                    " class=\"duplicateLayout\"", " class=\"duplicateKey\"", " class=\"duplicateValue\"");
            String filesHtml = getHtmlRowResult(RESULT_FILES, model.getFiles());
            String liHtml;
            if (SHOW_SIZE > i) {
                liHtml = "<li>";
            }
            else {
                liHtml = "<li class=\"duplicate\">";
            }
            String modelHtml = liHtml + "<table class=\"duplicateTable\">" +
                    md5Html + sizeHtml + filesHtml +
                    "</table></li>";
            resultHtml.append(modelHtml);
        }
        resultHtml.append("</ul>");
        if (models.size() > SHOW_SIZE) {
            resultHtml.append(String.format(HTML_BUTTON_SHOW, DUPLICATE_FOLDER_NAME, DUPLICATE_FOLDER_NAME));
            resultHtml.append(String.format(HTML_BUTTON_HIDE, DUPLICATE_FOLDER_NAME, DUPLICATE_FOLDER_NAME));
        }
        return resultHtml.toString();
    }
    private static String getPathListHtml(List<String> models) {
        StringBuilder pathListHtml = new StringBuilder(EMPTY_STRING);
        for (String strHtml : models) {
            pathListHtml.append(strHtml).append("<br>");
        }
        return pathListHtml.toString();
    }

    private static String getResultHtmlOfSuffix(List<ParamModelSuffix> models) {
        StringBuilder resultHtml = new StringBuilder(EMPTY_STRING);
        resultHtml.append("<ul>");
        for (int i = 0; i < models.size(); i++) {
            ParamModelSuffix model = models.get(i);
            String suffixHtml = getHtmlRowResult(SUFFIX_FOLDER_NAME, model.getSuffix(),
                    " class=\"suffixLayout\"", " class=\"suffixKey\"", " class=\"suffixValue\"");
            String totalSizeHtml = getHtmlRowResult(RESULT_TOTAL_SIZE, model.getTotalSize(),
                    " class=\"suffixLayout\"", " class=\"suffixKey\"", " class=\"suffixValue\"");
            String filesHtml;
            if (model.getSuffix().equalsIgnoreCase(FILE_TYPE_SO)) {
                filesHtml  = getHtmlRowSonSo(RESULT_FILES, model.getFiles());
            }
            else {
                filesHtml  = getHtmlRowSon(RESULT_FILES, model.getFiles());
            }
            String liHtml;
            if (SHOW_SIZE > i) {
                liHtml = "<li>";
            }
            else {
                liHtml = "<li class=\"suffix\">";
            }
            String modelHtml = liHtml + "<table class=\"suffixTable\">" +
                    suffixHtml + totalSizeHtml + filesHtml +
                    "</table></li>";
            resultHtml.append(modelHtml);
        }
        resultHtml.append("</ul>");
        if (models.size() > SHOW_SIZE) {
            resultHtml.append(String.format(HTML_BUTTON_SHOW, SUFFIX_FOLDER_NAME, SUFFIX_FOLDER_NAME));
            resultHtml.append(String.format(HTML_BUTTON_HIDE, SUFFIX_FOLDER_NAME, SUFFIX_FOLDER_NAME));
        }
        return resultHtml.toString();
    }

    private static String getResultHtmlFileSize(List<ParamModelFileSize> models) {
        StringBuilder resultHtml = new StringBuilder(EMPTY_STRING);
        resultHtml.append("<table class=\"fileSizeTable\">");
        String resultFieldHtml = getHtmlRowResult(FILE_SIZE_RESULT_FILE, FILE_SIZE_RESULT_SIZE,
                " class=\"fileSizeLayout\"", " class=\"fileSizeKey\"", " class=\"fileSizeValue\"");
        resultHtml.append(resultFieldHtml);
        for (int i = 0; i < models.size(); i++) {
            ParamModelFileSize model = models.get(i);
            String resultRowHtml;
            if (SHOW_SIZE > i) {
                resultRowHtml = getHtmlRowResult(model.getFile(), model.getSize(),
                        " class=\"fileSizeLayout\"", " class=\"fileSizeKey\"", " class=\"fileSizeValue\"");
            }
            else {
                resultRowHtml = getHtmlRowResult(model.getFile(), model.getSize(),
                        " class=\"fileSize\"", " class=\"fileSizeKey\"", " class=\"fileSizeValue\"");
            }
            resultHtml.append(resultRowHtml);
        }
        resultHtml.append("</table>");
        if (models.size() > SHOW_SIZE) {
            resultHtml.append(String.format(HTML_BUTTON_SHOW, FILE_SIZE_FOLDER_NAME, FILE_SIZE_FOLDER_NAME));
            resultHtml.append(String.format(HTML_BUTTON_HIDE, FILE_SIZE_FOLDER_NAME, FILE_SIZE_FOLDER_NAME));
        }
        return resultHtml.toString();
    }

    private static String getHtmlRow(String key, String valve) {
        return "<tr class=\"layout\"><td class=\"key\">" + key + "</td><td class=\"value\">" + valve + "</td></tr>";
    }

    private static String getHtmlRow(String key, long valve) {
        return "<tr class=\"layout\"><td class=\"key\">" + key + "</td><td class=\"value\">" + valve + "</td></tr>";
    }

    private static String getHtmlRowResultClass(String key, String valve) {
        return "<tr class=\"result\"><td class=\"key\">" + key + "</td><td class=\"value\">" + valve + "</td></tr>";
    }

    private static String getHtmlRowResult(String key, String valve, String trClass, String tdClassKey, String tdClassValue) {
        return String.format("<tr%s><td%s>" + key + "</td><td%s>" + valve + "</td></tr>", trClass, tdClassKey, tdClassValue);
    }

    private static String getHtmlRowResult(String key, long valve, String trClass, String tdClassKey, String tdClassValue) {
        return String.format("<tr%s><td%s>" + key + "</td><td%s>" + valve + "</td></tr>", trClass, tdClassKey, tdClassValue);
    }

    private static String getHtmlRowResult(String key, List<String> valve) {
        StringBuilder resValve = new StringBuilder(EMPTY_STRING);
        for (String ele: valve) {
            resValve.insert(0, "<li type=\"circle\">" + ele + "</li>");
        }
        return "<tr class=\"duplicateLayout\"><td class=\"duplicateKey\">" + key + "</td><td class=\"duplicateValue\"><ul>" + resValve + "</ul></td></tr>";
    }

    private static String getHtmlRowSon(String key, List<ParamModelSuffixSon> files) {
        StringBuilder resValve = new StringBuilder(EMPTY_STRING);
        for(ParamModelSuffixSon so:files){
            resValve.insert(0, RESULT_FILE + ":" + so.getFile() + "<br>");
            resValve.insert(0, RESULT_SIZE + ":" + so.getSize() + "<br>");
        }
        return "<tr class=\"suffixLayout\"><td class=\"suffixKey\">" + key + "</td><td class=\"suffixValue\">" + resValve + "</td></tr>";
    }
    private static String getHtmlRowSonSo(String key, List<ParamModelSuffixSo> files) {
        StringBuilder resValve = new StringBuilder(EMPTY_STRING);
        for(ParamModelSuffixSo so:files){
            resValve.insert(0, RESULT_FILE +":" + so.getFile() + "<br>");
            resValve.insert(0, RESULT_SIZE + ":" + so.getSize() + "<br>");
            resValve.insert(0, RESULT_COMPRESS + ":" + so.getCompress() + "<br>");
        }
        return "<tr class=\"suffixLayout\"><td class=\"suffixKey\">" + key + "</td><td class=\"suffixValue\">" + resValve + "</td></tr>";
    }


    private static String getCurrentTime() {
        long currentTimeMillis = System.currentTimeMillis();
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(currentTimeMillis);
    }

    private static void unpackHap(String srcPath, String outPath) throws BundleException {
        try (FileInputStream fis = new FileInputStream(srcPath);
             ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(fis))) {
            File destDir = new File(outPath);
            if (!destDir.exists()) {
                destDir.mkdirs();
            }

            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                String entryName = entry.getName();
                File entryFile = new File(outPath, entryName);

                if (entry.isDirectory()) {
                    entryFile.mkdirs();
                    zipInputStream.closeEntry();
                    continue;
                }
                File parent = entryFile.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }

                FileOutputStream fos = new FileOutputStream(entryFile);
                byte[] buffer = new byte[BUFFER_SIZE];
                int bytesRead;
                while ((bytesRead = zipInputStream.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }
                fos.close();
                zipInputStream.closeEntry();
            }
        } catch (IOException e) {
            LOG.error("unpack hap failed IOException " + e.getMessage());
            throw new BundleException("unpack hap failed IOException " + e.getMessage());
        }
    }

    private String md5HashCode(String filePath) {
        String md5 = "";
        try {
            InputStream fis = new FileInputStream(filePath);
            MessageDigest md = MessageDigest.getInstance(MD5);
            byte[] buffer = new byte[MD5_BUFFER_SIZE];
            int length = -1;
            while ((length = fis.read(buffer, 0, MD5_BUFFER_SIZE)) != -1) {
                md.update(buffer, 0, length);
            }
            fis.close();
            byte[] md5Bytes = md.digest();
            BigInteger bigInt = new BigInteger(1, md5Bytes);
            md5 = bigInt.toString(16);
        } catch (Exception exception) {
            LOG.error("Scan::md5HashCode exception" + exception);
        }
        return md5;
    }

    /**
     * delete file
     *
     * @param path file path which will be deleted
     */
    private static void deleteFile(final String path) {
        File file = new File(path);
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (int i = 0; i < files.length; i++) {
                    deleteFile(files[i].toString());
                }
            }
            file.delete();
        }
    }

    private static void writeFile(String targetPath, String data) throws IOException {
        FileWriter fileWriter = new FileWriter(targetPath);
        fileWriter.write(data);
        fileWriter.flush();
        fileWriter.close();
    }

}
